# How to Deploy on Hugging Face

1) Move the "assets" directory and llamabot.js, package.json files into your HuggingFace repo directory.

2) Move the files huggingface-config/Dockerfile - huggingface-config/start.sh - huggingface-config/startServices.json into the root of the hugging face repo.

3) Edit Docker file as you need, the dockerfile is set to automatically download Vicuna 1.1 7B

3) Move default.env into your repo as .env and edit for your needs

4) Push the changes

You should then see the bot being built and deployed on HuggingFace

pm2 log will run automatically so you can see frontend and backend logs.

PLEASE NOTE: Your hugging face repo should remain private!
